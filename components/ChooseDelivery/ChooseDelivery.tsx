import { BaseSyntheticEvent, FormEventHandler, useEffect, useState } from 'react';
import type { NextPage } from 'next';
import { company, lorem } from 'faker';
import {
  HomeDeliveryOptionsContainer,
  HomeFilterContainer,
  HomeFilterFormField,
  HomeFilterInput,
  HomeFilterLabel,
  HomeTitle,
} from './ChooseDelivery.styles';
import DeliveryOption from './DeliveryOption/DeliveryOption';

interface DeliveryOption {
  name: string;
  description: string;
}

function orderAsc(a: string, b: string) {
  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
}

function generateDeliveryOptions(length: number): DeliveryOption[] {
  return Array.from({ length }, () => ({
    name: company.companyName(),
    description: lorem.sentences(2),
  })).sort((a, b) => orderAsc(a.name, b.name));
}

const ChooseDelivery: NextPage = () => {
  // TODO: move to i18n file
  const nameLabel = 'Name';
  const descriptionLabel = 'Description';

  const [currentName, setCurrentName] = useState<string>('');
  const [currentDescription, setCurrentDescription] = useState<string>('');
  const [deliveryOptions, setDeliveryOptions] = useState<DeliveryOption[]>([]);
  const [filteredDeliveryOptions, setFilteredDeliveryOptions] = useState<DeliveryOption[]>([]);

  useEffect(() => {
    const mockedDeliveryOptions = generateDeliveryOptions(12);
    setDeliveryOptions([...mockedDeliveryOptions]);
    setFilteredDeliveryOptions([...mockedDeliveryOptions]);
  }, []);

  const filterDeliveryOptions = (currentName: string, currentDescription: string): DeliveryOption[] => {
    return deliveryOptions.filter(({ name, description }) => {
      const containsCurrentName = name.toLowerCase().includes(currentName);
      const containsCurrentDescription = description.toLowerCase().includes(currentDescription);
      return containsCurrentName && containsCurrentDescription;
    });
  };

  const onNameInputChange: FormEventHandler<HTMLInputElement> = (inputEvent: BaseSyntheticEvent) => {
    const inputElement = inputEvent.target as HTMLInputElement;
    const inputValue = inputElement.value.toLowerCase();
    setCurrentName(inputValue);
    setFilteredDeliveryOptions(filterDeliveryOptions(inputValue, currentDescription));
  };

  const onDescriptionInputChange = (inputEvent: BaseSyntheticEvent) => {
    const inputElement = inputEvent.target as HTMLInputElement;
    const inputValue = inputElement.value.toLowerCase();
    setCurrentDescription(inputValue);
    setFilteredDeliveryOptions(filterDeliveryOptions(currentName, inputValue));
  };

  return (
    <>
      <HomeTitle>Filters:</HomeTitle>
      <HomeFilterContainer>
        <HomeFilterFormField>
          <HomeFilterLabel>{nameLabel}:</HomeFilterLabel>
          <HomeFilterInput placeholder={nameLabel} onInput={onNameInputChange}></HomeFilterInput>
        </HomeFilterFormField>
        <HomeFilterFormField>
          <HomeFilterLabel>{descriptionLabel}:</HomeFilterLabel>
          <HomeFilterInput placeholder={descriptionLabel} onInput={onDescriptionInputChange}></HomeFilterInput>
        </HomeFilterFormField>
      </HomeFilterContainer>
      <HomeTitle>Deliveries:</HomeTitle>
      <HomeDeliveryOptionsContainer>
        {filteredDeliveryOptions &&
          filteredDeliveryOptions.map((option, index) => <DeliveryOption key={`DeliveryOption${index}`} {...option} />)}
      </HomeDeliveryOptionsContainer>
    </>
  );
};

export default ChooseDelivery;
