import { render, screen } from '@testing-library/react';
import ChooseDelivery from './ChooseDelivery';

const deliveryOptionTextMock = 'DeliveryOptionMock';
const DeliveryOptionMock = () => <div>{deliveryOptionTextMock}</div>;
jest.mock('./DeliveryOption/DeliveryOption', () => DeliveryOptionMock);

describe('ChooseDelivery', () => {
  beforeEach(() => {
    render(<ChooseDelivery />);
  });

  it('should have a title for the filter section', () => {
    const filterTitle = screen.getByText(/filters/i);
    expect(filterTitle).toBeInTheDocument();
  });

  it('should have a name input', () => {
    const nameInput = screen.getByPlaceholderText(/name/i);
    expect(nameInput).toBeInTheDocument();
  });

  it('should have a description input', () => {
    const descriptionInput = screen.getByPlaceholderText(/description/i);
    expect(descriptionInput).toBeInTheDocument();
  });

  it('should have a title for the deliveries section', () => {
    const deliveriesTitle = screen.getByText(/deliveries/i);
    expect(deliveriesTitle).toBeInTheDocument();
  });

  it('should have 12 deliveries', () => {
    const deliveries = screen.getAllByText(deliveryOptionTextMock);
    expect(deliveries).toHaveLength(12);
  });
});
