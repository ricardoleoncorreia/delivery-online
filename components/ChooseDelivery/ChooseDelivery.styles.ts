import { globalTheme } from '@/core/styles/theme.styles';
import styled from 'styled-components';

export const HomeTitle = styled.h2`
  margin: 20px;
  font-size: ${globalTheme.text.title};
`;

export const HomeFilterContainer = styled.section`
  display: flex;
  flex-wrap: wrap;
  justify-content: var(--choose-delivery__filter-container__justify-content);
  gap: var(--choose-delivery__filter-container__gap);
  padding: 16px 40px;
  background-color: ${globalTheme.colors.gray};
`;

export const HomeFilterFormField = styled.section`
  display: flex;
  flex-direction: column;
`;

export const HomeFilterLabel = styled.label`
  margin-bottom: 4px;
  font-size: ${globalTheme.text.regular};
  font-weight: bold;
`;

export const HomeFilterInput = styled.input`
  min-height: 20px;
  padding: 4px 8px;
`;

export const HomeDeliveryOptionsContainer = styled.section`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: 20px;
  margin: 0 50px;
`;
