import type { NextPage } from 'next';
import {
  DeliveryOptionContainer,
  DeliveryOptionContent,
  DeliveryOptionContentDescription,
  DeliveryOptionContentLink,
  DeliveryOptionTitle,
} from './DeliveryOption.styles';

const DeliveryOption: NextPage<{ name: string; description: string }> = ({ name, description }) => {
  return (
    <DeliveryOptionContainer>
      <DeliveryOptionTitle>{name}</DeliveryOptionTitle>
      <DeliveryOptionContent>
        <DeliveryOptionContentDescription>{description}</DeliveryOptionContentDescription>
        <DeliveryOptionContentLink>Place order</DeliveryOptionContentLink>
      </DeliveryOptionContent>
    </DeliveryOptionContainer>
  );
};

export default DeliveryOption;
