import { render, screen } from '@testing-library/react';
import DeliveryOption from './DeliveryOption';

describe('DeliveryOption', () => {
  const name = 'name';
  describe(`when the name is "${name}"`, () => {
    const description = 'description';
    describe(`and the description is "${description}"`, () => {
      beforeEach(() => {
        render(<DeliveryOption name={name} description={description} />);
      });

      it('should have a name', () => {
        const nameElement = screen.getByText(name);
        expect(nameElement).toBeInTheDocument();
      });

      it('should have a description', () => {
        const descriptionElement = screen.getByText(description);
        expect(descriptionElement).toBeInTheDocument();
      });

      it('should have a "Place Order" link', () => {
        const placeOrderElement = screen.getByText(/Place Order/i);
        expect(placeOrderElement).toBeInTheDocument();
      });
    });
  });
});
