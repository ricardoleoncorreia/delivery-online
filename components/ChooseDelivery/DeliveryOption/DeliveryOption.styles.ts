import { globalTheme } from '@/core/styles/theme.styles';
import styled from 'styled-components';

const boxPadding = '4px';

export const DeliveryOptionContainer = styled.article`
  display: flex;
  flex-direction: column;
  width: 300px;
  border: 2px solid ${globalTheme.colors.darkGray};
`;

export const DeliveryOptionTitle = styled.h3`
  padding: ${boxPadding};
  color: ${globalTheme.colors.white};
  background-color: ${globalTheme.colors.blue};
`;

export const DeliveryOptionContent = styled.section`
  flex: 1;
  padding: ${boxPadding};
  background-color: ${globalTheme.colors.gray};
`;

export const DeliveryOptionContentDescription = styled.section``;

export const DeliveryOptionContentLink = styled.a`
  color: ${globalTheme.colors.green};
  font-weight: bold;
  cursor: pointer;
`;
