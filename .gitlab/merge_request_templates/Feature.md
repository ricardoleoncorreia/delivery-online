## Related issues

Closes #ticket_number

## What changed?

Brief summary of bug fixes or new functionality

## Notes

Particular considerations to take into account

## Conformity

- [ ] Added unit tests.
- [ ] Manually tested in Google Chrome & Mozilla Firefox.
- [ ] Endpoint added to Swagger.
- [ ] Endpoint tested with an API client.
- [ ] Project version updated.

-OR-

No applicable conformity items for this merge request.

/label ~feature ~infrastructure ~security ~documentation ~enhancement ~codebase
/assign @username
/milestone %milestone
