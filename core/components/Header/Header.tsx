import type { NextPage } from 'next';
import { HeaderContainer, HeaderStatus, HeaderStatusItem, HeaderTitle } from './Header.styles';

const Header: NextPage = () => {
  return (
    <HeaderContainer>
      <HeaderTitle>Delivery Online</HeaderTitle>
      <HeaderStatus>
        <HeaderStatusItem isActive={true}>1. Choose your delivery</HeaderStatusItem>
        <HeaderStatusItem isActive={false}>2. Place your order</HeaderStatusItem>
        <HeaderStatusItem isActive={false}>3. Complete your personal data</HeaderStatusItem>
      </HeaderStatus>
    </HeaderContainer>
  );
};

export default Header;
