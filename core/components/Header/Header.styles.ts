import styled from 'styled-components';
import { globalTheme } from '@/core/styles/theme.styles';

const statusBorderRadius = '5px';

export const HeaderContainer = styled.header`
  display: flex;
  flex-direction: var(--header__header-container__flex-direction);
  justify-content: space-between;
  align-items: center;
  gap: 20px;
  padding: var(--header__header-container__vertical-padding) 40px;
  min-height: 50px;
  background-color: ${globalTheme.colors.darkGray};
`;

export const HeaderTitle = styled.h1`
  color: ${globalTheme.colors.white};
  font-size: ${globalTheme.text.brand};
`;

export const HeaderStatus = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;

  p {
    &:nth-child(1) {
      border-top-left-radius: ${statusBorderRadius};
      border-bottom-left-radius: ${statusBorderRadius};
    }

    &:nth-child(3) {
      border-top-right-radius: ${statusBorderRadius};
      border-bottom-right-radius: ${statusBorderRadius};
    }
  }
`;

export const HeaderStatusItem = styled.p<{ isActive: boolean }>`
  padding: 2px 10px;
  color: ${(props) => (props.isActive ? globalTheme.colors.darkGray : globalTheme.colors.white)};
  background-color: ${(props) => (props.isActive ? globalTheme.colors.gray : globalTheme.colors.green)};
  font-size: ${globalTheme.text.regular};
  text-align: center;
`;
