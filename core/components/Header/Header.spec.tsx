import { globalTheme } from '@/core/styles/theme.styles';
import { render, screen } from '@testing-library/react';
import Header from './Header';

const activeStyles = { color: globalTheme.colors.darkGray, backgroundColor: globalTheme.colors.gray };
const inactiveStyles = { color: globalTheme.colors.white, backgroundColor: globalTheme.colors.green };

describe('Header', () => {
  let renderedContainer: HTMLElement;

  beforeEach(() => {
    const { container } = render(<Header />);
    renderedContainer = container;
  });

  it('should contain the name of the brand', () => {
    const brand = screen.getByText(/delivery online/i);
    expect(brand).toBeInTheDocument();
  });

  it('should contain three statuses', () => {
    const statuses = renderedContainer.querySelectorAll('p');
    expect(statuses).toHaveLength(3);
  });

  it('should have a "choose your delivery" status', () => {
    const chooseDelivery = screen.getByText(/1\. choose your delivery/i);
    expect(chooseDelivery).toBeInTheDocument();
  });

  it('should have a "place your order" status', () => {
    const placeOrder = screen.getByText(/2\. place your order/i);
    expect(placeOrder).toBeInTheDocument();
  });

  it('should have a "complete your personal data" status', () => {
    const completeData = screen.getByText(/3\. complete your personal data/i);
    expect(completeData).toBeInTheDocument();
  });

  describe('when the header is in its default state', () => {
    it('should set the first state as active', () => {
      const chooseDelivery = screen.getByText(/1\. choose your delivery/i);
      expect(chooseDelivery).toHaveStyle(activeStyles);
    });

    it('should set the second state as inactive', () => {
      const placeOrder = screen.getByText(/2\. place your order/i);
      expect(placeOrder).toHaveStyle(inactiveStyles);
    });

    it('should set the third state as inactive', () => {
      const completeData = screen.getByText(/3\. complete your personal data/i);
      expect(completeData).toHaveStyle(inactiveStyles);
    });
  });
});
