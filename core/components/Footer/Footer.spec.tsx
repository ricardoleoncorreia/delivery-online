import { render, screen } from '@testing-library/react';
import Footer from './Footer';

describe('Footer', () => {
  beforeEach(() => {
    render(<Footer />);
  });

  it('should contain the year the app was made', () => {
    const copyrightElement = screen.getByText(/2021/);
    expect(copyrightElement).toBeInTheDocument();
  });

  it('should contain the current year', () => {
    const currentYear = new Date().getFullYear();
    const yearRegex = new RegExp(`${currentYear}`);

    const copyrightElement = screen.getByText(yearRegex);

    expect(copyrightElement).toBeInTheDocument();
  });

  it(`should contain author's name`, () => {
    const copyrightElement = screen.getByText(/Ricardo León Correia/);

    expect(copyrightElement).toBeInTheDocument();
  });
});
