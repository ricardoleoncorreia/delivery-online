import type { NextPage } from 'next';
import { FooterContainer } from './Footer.styles';

const Footer: NextPage = () => {
  const initialYear = 2021;
  const currentYear = new Date().getFullYear();
  const yearRange = initialYear === currentYear ? `${initialYear}` : `${initialYear}-${currentYear}`;

  return (
    <FooterContainer>
      <p>&#169;{yearRange} Ricardo León Correia</p>
    </FooterContainer>
  );
};

export default Footer;
