import styled from 'styled-components';
import { globalTheme } from '@/core/styles/theme.styles';

export const FooterContainer = styled.footer`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  color: ${globalTheme.colors.white};
  background-color: ${globalTheme.colors.darkGray};
`;
