export const globalTheme = {
  colors: {
    lightBlue: '#EFF4F8',
    blue: '#0068D1',
    green: '#1DBB93',
    darkGray: '#2A3F54',
    gray: '#EBF0F3',
    red: '#EB5947',
    white: '#ffffff',
  },
  text: {
    regular: '12px',
    subtitle: '14px',
    title: '20px',
    brand: '28px',
  },
};
