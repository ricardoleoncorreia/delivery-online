import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  html {
    font-size: 12px;
    scroll-behavior: smooth;
  }

  body {
    font-family: 'DM Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  #__next {
    min-height: 100vh;
    display: flex;
    flex-direction: column;

    > main {
      flex: 1;
      margin: 24px;
    }
  }
`;
