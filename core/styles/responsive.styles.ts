import { createGlobalStyle } from 'styled-components';

const smallBreakpoint = `860px`;
const mediumBreakpoint = `1280px`;

export const GlobalResponsiveVariables = createGlobalStyle`
  :root {
    @media screen and (max-width: ${smallBreakpoint}) {
      --choose-delivery__filter-container__justify-content: center;
      --choose-delivery__filter-container__gap: 20px;

      --header__header-container__flex-direction: column;
      --header__header-container__vertical-padding: 16px;
    }

    @media screen and (min-width: ${smallBreakpoint}) and (max-width: ${mediumBreakpoint}) {
      --choose-delivery__filter-container__justify-content: default;
      --choose-delivery__filter-container__gap: 60px;

      --header__header-container__flex-direction: row;
      --header__header-container__vertical-padding: 0;
    }

    @media screen and (min-width: ${mediumBreakpoint}) {
      --choose-delivery__filter-container__justify-content: default;
      --choose-delivery__filter-container__gap: 60px;

      --header__header-container__flex-direction: row;
      --header__header-container__vertical-padding: 0;
    }
  }
`;
