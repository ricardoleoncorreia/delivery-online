import type { NextPage } from 'next';
import ChooseDelivery from '@/components/ChooseDelivery/ChooseDelivery';

const Index: NextPage = () => {
  return <ChooseDelivery />;
};

export default Index;
