import { AppProps } from 'next/app';
import Head from 'next/head';
import { GlobalResponsiveVariables } from '@/core/styles/responsive.styles';
import { GlobalStyles } from '@/core/styles/global.styles';
import { globalTheme } from '@/core/styles/theme.styles';
import Footer from '@/core/components/Footer/Footer';
import Header from '@/core/components/Header/Header';

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Head>
        <link rel='icon' href='/favicon.ico' />

        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name='theme-color' content={globalTheme.colors.darkGray} />
        <meta name='description' content='We take your favorite food to your place' />

        <title>Delivery Online</title>
      </Head>
      <GlobalResponsiveVariables />
      <GlobalStyles />
      <Header />
      <main>
        <Component {...pageProps} />
      </main>
      <Footer />
    </>
  );
}
